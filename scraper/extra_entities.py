#!/usr/bin/env python3
# This file is part of Pepper&Carrot API, which is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.

# The software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.

# You should have received a copy of the GNU General Public License along with this software.
# If not, see <https://www.gnu.org/licenses/>.

import yaml


def _load():
	global donation_methods, people
	with open("./information.yaml", "r") as fp:
		info             = yaml.safe_load(fp)
		donation_methods = info["donation methods"]
		people           = info["people"]

_load()


def _name_to_id(category, name):
	return category.get(name, default={"@id":None})["@id"]

def don_meth_name_to_id(name):
	return _name_to_id(donation_methods)

def person_name_to_id(name):
	return _name_to_id(people)


def _all(category):
	return [
		{
			"name": name,
			**properties
		}
		for name, properties in category.items()
	]

def all_people():
	return _all(people)

def all_donation_methods():
	return _all(donation_methods)

if __name__ == "__main__":
	import json
	import sys
	print("PEOPLE")
	json.dump(
		all_people(),
		sys.stdout,
		indent="\t",
		ensure_ascii=False
	)
	print("\n\nDONATION METHODS")
	json.dump(
		all_donation_methods(),
		sys.stdout,
		indent="\t",
		ensure_ascii=False
	)
