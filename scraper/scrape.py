#!/usr/bin/env python3
# This file is part of Pepper&Carrot API, which is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.

# The software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.

# You should have received a copy of the GNU General Public License along with this software.
# If not, see <https://www.gnu.org/licenses/>.

import sys
from os import path
import re
import json
import yaml
from functools import lru_cache as memoized
from glob import glob
from xml.etree import ElementTree
from typing import Dict, Union, Optional, Callable, TypeVar
import fuzzycount


T = TypeVar("T")
U = TypeVar("U")
def maybe(function: Callable[[T], U], value: Optional[T]) -> Optional[U]:
	return (
		function(value)
		if value is not None else
		None
	)


def reexcept(attempt, note, exception=Exception, filter=lambda err: True):
	try:
		return attempt()
	except exception as e:
		if filter(e):
			if isinstance(note, Exception):
				raise type(note)(*note.args, *e.args)
			else:
				print("{}: {}".format(note, e.args), file=sys.stderr)
		else:
			raise


def noexcept(attempt, exception=Exception, filter=lambda err: True, replacement=None):
	try:
		return attempt()
	except exception as e:
		if filter(e):
			return replacement
		else:
			raise


def globber(*pattern_path):
	'''
	Return a list of all the normalized paths expanded with glob from the reassembled path pattern
	'''
	return [
		path.normpath(p)
		for p
		in glob( path.join(*pattern_path) )
	]


def other_extension(filename, ext):
	"""
	Build a filename with the same base but another extension.
	"""
	return path.splitext(filename)[0] + ext


def episode_filenames(episode_dir, prefix="gfx_"):
	'''
	Return the filenames for an episode, stripped of a prefix
	'''
	return [
		path.basename(filename)[len(prefix):]
		for filename
		in sorted( globber(episode_dir, "lang", prefix + "*.*") )
	]


def grouped_filenames(filenames):
	'''
	Decide which pages are of which nature, based on position in the list
	'''
	return {
		"cover":   filenames[0],    # first
		"title":   filenames[1],    # second
		"comic":   filenames[2:-1], # third to before-last
		"credits": filenames[-1]    # last
	} if len(filenames) >= 2 else {
		"cover":   None,
		"title":   None,
		"comic":   [],
		"credits": None
	}


@memoized(maxsize=1)
def grouped_episode_filenames(episode_dir):
	return grouped_filenames( episode_filenames(episode_dir) )


@memoized(maxsize=1)
def episode_languages(episode_dir):
	'''
	Return the names of all directories in the "lang" directory of the episode
	'''
	return [
		path.basename(lang_dir)
		for lang_dir
		in sorted( globber(episode_dir, "lang", "*/") )
	]


def without_episode_prefix(title, lang):
	'''
	Return the episode's title without the "Episode XX: " part
	'''

	# FIXME Don't conditionalize, parametrize

	# Lojban is making it hard on us
	if lang == "jb":
		m = re.search(r"moi fa (?:la'e lu|la'au) (.*) li'u$", title)
		if m:
			return m.group(1)

		m = re.search(r"moi fa (.*)$", title)
		if m:
			return m.group(1)

		return None

	# For all other languages this should work
	else:
		return re.split(r":|：", title)[-1].strip()


def episode_title(svg_filepath, language):
	return without_episode_prefix(noexcept(
		lambda: "".join([
			text
			for text in sum(
				[
					[ element.text, element.tail ]
					for element
					in ElementTree.parse(svg_filepath).find('.//*[@id="episode-title"]').iter()
				],
				[]
			)
			if text is not None
		]).strip()
	), language)


def one_or_nothing(list):
	return list[0] if len(list) == 1 else None

def same_or_nothing(value1, value2):
	# Strict
	return value1 if value1 == value2 else None
	# Loose
	# return value1 if value1.replace(" ", "").replace(" ", "").lower() == value2.replace(" ", "").replace(" ", "").lower() else None

def coalesce(*items):
	return next(
		(i for i in items if i is not None),
		None
	)


def episode_translations(ep_id, ep_dir):
	'''
	Returns a dictionary of language → translation information
	'''
	return {
		language: {
			"@id": ep_id + "/" + language,
			"title": episode_title( globber(ep_dir, "lang", language, "E??.svg")[0], language ),
			"cover": other_extension(
				"{{SOURCES_BASE_URL}}{name}/low-res/{lang}_{base}".format(
					name=path.basename(ep_dir),
					lang=language,
					base=grouped_episode_filenames(ep_dir)["cover"]
				),
				".jpg"
			)
		}
		for language in episode_languages(ep_dir)
	}


def substitute_keys(d, key_translation):
	'''
	Return a dictionary that contains only key/value pairs from d where the key appears in
	key_translation. If key's value in key_translation is not True, use that key instead of the
	original one.
	'''
	return {
		(k if key_translation[k] == True else key_translation[k]): v
		for k, v in d.items()
		if k in key_translation and key_translation[k]
	}


def load_json(filename):
	with open(filename, "r") as fp:
		return json.load(fp)


def load_yaml(filename):
	with open(filename, "r") as fp:
		return yaml.safe_load(fp)


@memoized(maxsize=1)
def episode_yaml_information(ep_dir):
	return load_yaml(
		path.join(ep_dir, "info.yaml")
	)


def episode_serial(ep_dir):
	return episode_yaml_information(ep_dir)["serial"]


def episode_id(ep_dir):
	return episode_yaml_information(ep_dir)["id"]


def language_matches(langid, langcode):
	'''
	Whether langcode (2 letter Pepper&Carrot language code) matches langid (the ID used in the
	published data)
	'''
	return langid.endswith("/" + langcode)


def total_supporters(supporters: Dict[str, Union[str, int]]):
	return coalesce(
		supporters.get("total"),
		maybe(fuzzycount.aggregate, fuzzycount.sum([
			v
			for k, v in supporters.items()
			if k != "total"
		]))
	)


def episode_metadata(ep_dir, languages, extra_information):
	'''
	Return metadata for each episode in the directory we get as a parameter
	'''

	ep_id = "{{BASE_URL}}episodes/{id}".format(id=episode_id(ep_dir))
	ep_serial = episode_serial(ep_dir)
	ep_yaml_info = episode_yaml_information(ep_dir)

	wikidata = noexcept(lambda: extra_information["episodes"][ep_serial]["wikidata"])

	return {
		"@context":     "{BASE_URL}contexts/episode.jsonld",
		"@id":          ep_id,
		"serial":       ep_serial,
		"wikidata":     ("http://www.wikidata.org/entity/" + wikidata) if wikidata else None,
		"nr_pages":     len( grouped_episode_filenames(ep_dir)["comic"] ) + 1, # add 1 for the title
		"translations": episode_translations(ep_id, ep_dir),

		**substitute_keys(
			ep_yaml_info,
			{
				"published": True, # True means "keep but don't translate key" here
				"url":       "old_style_url",
			}
		),

		"financial_supporters": [
			{
				"donation_method": extra_information["donation methods"][donation_method]["@id"],
				"amount_of_backers": num
			}
			for donation_method, num in ep_yaml_info["financial supporters"].items()
			if donation_method in extra_information["donation methods"]
		],

		# TODO If all exact numbers are known, add them and put the number here.
		# TODO When there are uncertainties in the amount of backers, maybe accumulate the fault and show an approximate total tally
		"total_financial_supporters": total_supporters(ep_yaml_info["financial supporters"]),

		"original_language": "{BASE_URL}languages/" + ep_yaml_info["original language"],

		"credits": [] # TODO
	}


def episodes_metadata(languages, webcomics_dir, extra_information):
	'''
	Return metadata for each episode in the directory we get as a parameter
	'''
	return list(
		sorted(
			(
				episode_metadata(ep_dir, languages, extra_information)
				for ep_dir in sorted(globber(webcomics_dir, "ep*/"))
				if episode_yaml_information(ep_dir).get("published")
			),
			key=lambda x: x["serial"]
		)
	)


if __name__ == "__main__":
	if len(sys.argv) != 2 or sys.argv[1] in ["-h", "--help"]:
		print(sys.argv[0] + " <webcomics_dir>", file=sys.stderr)
		sys.exit(1)

	# Dump, in JSON format…
	json.dump(
		# …the episodes' metadata generated from the episodes directory that we take as the first argument of the script…
		episodes_metadata(
			languages=load_json("../testdata/languages.json"),
			webcomics_dir=sys.argv[1],
			extra_information=load_yaml("../information.yaml")
		),
		sys.stdout,         # …to the standard output…
		indent="\t",        # …indenting the JSON with a tab…
		ensure_ascii=False, # …keeping foreign scripts readable in the file…
		default=str         # …and just serializing unserializable stuff as strings (notably dates).
	)
	print()
