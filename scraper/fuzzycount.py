#!/usr/bin/env python3
# This file is part of Pepper&Carrot API, which is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.

# The software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.

# You should have received a copy of the GNU General Public License along with this software.
# If not, see <https://www.gnu.org/licenses/>.


import sys
import builtins
from typing import cast, Union, Optional, Tuple, Iterable, Sequence, TypeVar


def fuzzy_parsed(element: str) -> Optional[Tuple[float, float]]:
	try:
		center = int(element[1:])
		return (center * 0.9, center * 1.1)
	except ValueError:
		return None


def range_parsed(element: str) -> Optional[Tuple[int, int]]:
	split = element.split("-")
	try:
		return \
			(int(split[0]), int(split[1])) \
			if len(split) == 2 else \
			None
	except ValueError:
		return None


def exact_parsed(element: str) -> Optional[Tuple[int, int]]:
	try:
		inted = int(element)
		return (inted, inted)
	except ValueError:
		return None


def parsed(element: Union[int, str]) -> Optional[Tuple[float, float]]:
	return (
		(element, element)    if isinstance(element, int) else
		range_parsed(element) if "-" in element else
		fuzzy_parsed(element) if element.startswith("~") else
		exact_parsed(element)
	)


T = TypeVar("T")
def none_if_any_none(seq: Sequence[Optional[T]]) -> Optional[Sequence[T]]:
	return (
		None
		if any(x is None for x in seq) else
		cast(Sequence[T], seq)  # Tell mypy that seq doesn't contain None here
	)


def sum(elements: Iterable[Union[int, str]]) -> Optional[Tuple[int, int]]:
	bounds = none_if_any_none([
		parsed(element)
		for element in elements
	])

	return (
		int(builtins.sum(x[0] for x in bounds)),
		int(builtins.sum(x[1] for x in bounds))
	) if bounds is not None else None


def aggregate(count: Tuple[int, int]) -> Union[str, int]:
	return (
		count[0] if count[0] == count[1] else
		"{}-{}".format(count[0], count[1])
	)


if __name__ == "__main__":
	print(sum(sys.stdin))
