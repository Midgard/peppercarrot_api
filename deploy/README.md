# Deployment

⚠ The application is not yet stable. Configuration may change drastically without notice.

Only supported on *NIXes, like Linux and macOS. Running on Windows may be possible, but won't ever
be supported.

These instructions assume you kind of know what you're doing. If you have no idea what WSGI is, you
may want to read up on that first. If you have no idea what `cd` is, stop now.

 1. Make sure Python 3 is installed. For better performance, you can use [PyPy](https://pypy.org/)
    instead of CPython.
 2. Install [pipenv](https://pypi.org/project/pipenv/).
 3. Create a user `peppercarrotapi`. Example command:
    `sudo adduser --system --home /var/local/peppercarrot-api peppercarrotapi`
 4. Create a directory `/opt/peppercarrot-api/`.
 5. Clone this repository as `/opt/peppercarrot-api/application` and cd into it.
 6. If you want to run with PyPy, run `pipenv --python /usr/bin/pypy3`. Else, skip this step.
 7. Run `pipenv install`.
 8. Choose and install a WSGI server. We use [gunicorn](https://gunicorn.org/). Familiarize
    yourself with deployment on your WSGI server.
 9. Set up a supervisor. We use [runit](http://smarden.org/runit/). Create a service to run the
    WSGI application with the appropriate environment variables (see below). An example service
    file for runit, using gunicorn as WSGI server, is included in `deploy/run`.
10. Put an HTTP reverse proxy in front of your WSGI server. We use [`nginx`](https://nginx.org/).
    Check your WSGI server's deployment documentation and your reverse proxy's documentation for
    more information. An example nginx configuration snippet is included in `deploy/nginx.conf`.
11. Start everything. If you have questions, contact M1dgard on [Freenode](https://freenode.net/)
    IRC or open an issue at https://framagit.org/Midgard/peppercarrot_api/issues .

## Environment variables

* `PCA_DATA_DIR` Path to directory with the data files. Defaults to `./testdata/`
* `PCA_BASE_URL` URL where the API is accessible from ("what you type in the browser").
  Defaults to `http://127.0.0.1:8000/`
* `PCA_SOURCES_BASE_URL` URL where the artwork is available from.
  Defaults to `https://www.peppercarrot.com/0_sources/`
