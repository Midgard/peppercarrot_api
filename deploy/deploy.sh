#!/bin/bash
# This file is part of Pepper&Carrot API, which is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.

# The software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.

# You should have received a copy of the GNU General Public License along with this software.
# If not, see <https://www.gnu.org/licenses/>.

REVISION=origin/master


cd "$(dirname "$0")/.."

git fetch &&
	git reset --hard "$REVISION" &&
	sudo -u peppercarrotapi -g nogroup env PIPENV_VENV_IN_PROJECT=yes pipenv install &&
	sudo sv restart peppercarrot-api
