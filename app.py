# This file is part of Pepper&Carrot API, which is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.

# The software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.

# You should have received a copy of the GNU General Public License along with this software.
# If not, see <https://www.gnu.org/licenses/>.

import os
import falcon
import json


def env_var(name, default):
	val = os.environ.get(name)
	if val is None:
		print("Using default value for {}: {}".format(name, default))
		return default
	else:
		print("Picked up environment variable {}: {}".format(name, val))
		return val


API_SOURCE_CODE  = env_var("PCA_API_SOURCE_CODE",  "https://framagit.org/Midgard/peppercarrot_api")
API_LICENSE_TEXT = env_var("PCA_API_LICENSE_TEXT", "This service is powered by free software. The source code is available at " + API_SOURCE_CODE)

DATA_DIR         = env_var("PCA_DATA_DIR",         "./testdata/")
BASE_URL         = env_var("PCA_BASE_URL",         "http://127.0.0.1:8000/")
SOURCES_BASE_URL = env_var("PCA_SOURCES_BASE_URL", "https://www.peppercarrot.com/0_sources/")


def from_json(filename):
	with open(os.path.join(DATA_DIR, filename), "r") as fp:
		return json.loads(
			fp.read()
				.replace("{BASE_URL}", BASE_URL)
				.replace("{SOURCES_BASE_URL}", SOURCES_BASE_URL)
		)


class HomeResource:
	def on_get(self, req, resp):
		resp.media = {
			"donation_methods": BASE_URL+"donation_methods",
			"episodes":         BASE_URL+"episodes",
			"languages":        BASE_URL+"languages",
			"people":           BASE_URL+"people",
			"source_code":      API_SOURCE_CODE,
			"license_text":     API_LICENSE_TEXT,
		}


class Resource:
	def __init__(self, filename):
		self.data = from_json(filename)

	def on_get(self, req, resp):
		resp.media = self.data


api = falcon.API()
api.add_route("/",                 HomeResource())
api.add_route("/donation_methods", Resource("donation_methods.json"))
api.add_route("/episodes",         Resource("episodes.json"))
api.add_route("/languages",        Resource("languages.json"))
api.add_route("/people",           Resource("people.json"))
