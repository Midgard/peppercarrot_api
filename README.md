# Pepper&Carrot RESTful API and Linked Data [<img src="/.repo/agplv3-155x51.png" width="77" height="25" alt="licensed under the AGPL"/>](/LICENSE)

This is a project to create a truly
[RESTful](https://en.wikipedia.org/wiki/Representational_state_transfer) (in the strictest sense of
the word) [Pepper&Carrot](https://www.peppercarrot.com/) API that serves [Linked
Data](https://en.wikipedia.org/wiki/Linked_data).

The focus for the first milestone is creating an API that only returns
[JSON-LD](https://json-ld.org/). Later, RDF, Turtle and HTML representations can be added. Maybe
one day this project could power the core of the Pepper&Carrot website, who knows?

## Examples of target behaviour

Examples of target output for the first milestone, using the domain peppercarrot.com:

https://peppercarrot.com
```json
{
	"languages": "https://peppercarrot.com/languages",
	"episodes": "https://peppercarrot.com/episodes",
	"people": "https://peppercarrot.com/episodes"
}
```

https://peppercarrot.com/episodes
```json
[
	{
		"@context": "https://peppercarrot.com/contexts/episode.jsonld",
		"@id": "https://peppercarrot.com/episodes/1",
		"nr_pages": 4,
		"original_language": "https://peppercarrot.com/langs/en",
		"translations": {
			"https://peppercarrot.com/langs/en": {"@id": "https://peppercarrot.com/episodes/1/en", "title": "The Potion of Flight", "cover": "https://www.peppercarrot.com/0_sources/ep01_Potion-of-Flight/low-res/en_Pepper-and-Carrot_by-David-Revoy_E01.jpg"},
			"https://peppercarrot.com/langs/fr": {"@id": "https://peppercarrot.com/episodes/1/fr", "title": "Potion d'Envol", "cover": "https://www.peppercarrot.com/0_sources/ep01_Potion-of-Flight/low-res/fr_Pepper-and-Carrot_by-David-Revoy_E01.jpg"},
			…
		}
	},
	{
		"@context": "https://peppercarrot.com/contexts/episode.jsonld",
		"@id": "https://peppercarrot.com/episodes/2",
		"nr_pages": 6,
		"original_language": "https://peppercarrot.com/langs/fr",
		"translations": {
			"https://peppercarrot.com/langs/en": {"@id": "https://peppercarrot.com/episodes/2/en", "title": "Rainbow Potions", "cover": "https://www.peppercarrot.com/0_sources/ep02_Rainbow-potions/low-res/en_Pepper-and-Carrot_by-David-Revoy_E02.jpg"},
			"https://peppercarrot.com/langs/fr": {"@id": "https://peppercarrot.com/episodes/2/fr", "title": "Les potions arc-en-ciel", "cover": "https://www.peppercarrot.com/0_sources/ep02_Rainbow-potions/low-res/fr_Pepper-and-Carrot_by-David-Revoy_E02.jpg"},
			…
		}
	},
	…
]
```

https://peppercarrot.com/episodes/1
```json
{
	"@context": "https://peppercarrot.com/contexts/episode.jsonld",
	"@id": "https://peppercarrot.com/episodes/1",
	"nr_pages": 4,
	"original_language": "https://peppercarrot.com/langs/en",
	"translations": {
		"https://peppercarrot.com/langs/en": {"@id": "https://peppercarrot.com/episodes/1/en", "title": "The Potion of Flight", "cover": "https://www.peppercarrot.com/0_sources/ep01_Potion-of-Flight/low-res/en_Pepper-and-Carrot_by-David-Revoy_E01.jpg"},
		"https://peppercarrot.com/langs/fr": {"@id": "https://peppercarrot.com/episodes/1/fr", "title": "Potion d'Envol", "cover": "https://www.peppercarrot.com/0_sources/ep01_Potion-of-Flight/low-res/fr_Pepper-and-Carrot_by-David-Revoy_E01.jpg"},
			…
	}
}
```

https://peppercarrot.com/episodes/1/en
```json
{
	"@context": "https://peppercarrot.com/contexts/episode.jsonld",
	"@id": "https://peppercarrot.com/episodes/1/en",
	"language": "https://peppercarrot.com/languages/en",
	"title": "The Potion of Flight",
	"cover": "https://www.peppercarrot.com/0_sources/ep01_Potion-of-Flight/low-res/en_Pepper-and-Carrot_by-David-Revoy_E01.jpg",
	"pages": [
		{
			"@id": "https://peppercarrot.com/episodes/1/en/pages/cover",
			"@type": "https://peppercarrot.com/ontology/Cover",
			"graphics": {"low-res": "https://www.peppercarrot.com/0_sources/ep01_Potion-of-Flight/low-res/en_Pepper-and-Carrot_by-David-Revoy_E01P00.jpg"}
		},
		{
			"@id": "https://peppercarrot.com/episodes/1/en/pages/1",
			"@type": "https://peppercarrot.com/ontology/Page",
			"page_number": "1",
			"graphics": {"low-res": "https://www.peppercarrot.com/0_sources/ep01_Potion-of-Flight/low-res/en_Pepper-and-Carrot_by-David-Revoy_E01P01.jpg"}
		},
		{
			"@id": "https://peppercarrot.com/episodes/1/en/pages/2",
			"@type": "https://peppercarrot.com/ontology/Page",
			"page_number": "2",
			"graphics": {"low-res": "https://www.peppercarrot.com/0_sources/ep01_Potion-of-Flight/low-res/en_Pepper-and-Carrot_by-David-Revoy_E01P02.jpg"}
		},
		{
			"@id": "https://peppercarrot.com/episodes/1/en/pages/3",
			"@type": "https://peppercarrot.com/ontology/Page",
			"page_number": "3",
			"graphics": {"low-res": "https://www.peppercarrot.com/0_sources/ep01_Potion-of-Flight/low-res/en_Pepper-and-Carrot_by-David-Revoy_E01P03.jpg"}
		}
	]
}
```

https://peppercarrot.com/people
```json
[
	{
		"@context": "https://peppercarrot.com/contexts/person.jsonld",
		"@id": "https://peppercarrot.com/people/Deevad",
		"name": "David Revoy",
		"real_name": "David Revoy",
		"nickname": "Deevad",
		"url": "https://davidrevoy.com/",
		"framagit": "https://framagit.org/Deevad",
		"freenode": "deevad",
		"wikidata": "https://www.wikidata.org/wiki/Q20036576",
		"description": "founder of Pepper&Carrot, author of all the artwork"
	},
	{
		"@context": "https://peppercarrot.com/contexts/person.jsonld",
		"@id": "https://peppercarrot.com/people/Midgard",
		"name": "Midgard",
		"nickname": "Midgard",
		"framagit": "https://framagit.org/Midgard",
		"freenode": "M1dgard",
		"description": "Git guru, develops a small amount of auxiliary tools, Dutch translator"
	},
	…
]
```

Appropriate JSON-LD contexts and ontology concepts should be made, too.

## Development

Only supported on *NIXes, like Linux and macOS.

### PyPy

We're developing against [PyPy](https://pypy.org/), an alternative Python implementation that does
[JIT compilation](https://en.wikipedia.org/wiki/Just-in-time_compilation) for better performance.
It's best if you install PyPy (Python language version 3), but you can also use CPython (the
standard Python) version 3.5.

### Install

1. Install PyPy, or CPython 3.5.
2. Install [pipenv](https://pypi.org/project/pipenv/).
3. Clone this repository and cd into it.
4. Run `pipenv --python /usr/bin/pypy3` to set up the virtualenv through pipenv, using PyPy. Skip
   this step if you want to use CPython.
5. Run `pipenv install` in the working directory of this repo.
6. Run `./run-dev.sh` to run the site locally.

### Code style and editor

It's advisable to use an editor with [EditorConfig](https://editorconfig.org/) support to do
indentation the right way. Some editors ship with out-of-the-box support and some support it
through a plug-in.

We use tabs for indentation, disobeying PEP-8. If you have an EditorConfig-aware editor as
recommended above, you shouldn't need to worry about this.

## Deployment

See [deploy/README.md](./deploy/README.md).
